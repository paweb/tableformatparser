Konwerter danych tablearycznych między różnymi formatami
========================


Użycie:

    php app/console converter:file -f csv -t json table.csv

Dostępne formaty parserów definiowane są w services.yml:

        Madkom_console.parsers.class:
            json: Madkom\ConsoleBundle\Service\Parser\JsonParser
            csv: Madkom\ConsoleBundle\Service\Parser\CsvParser