<?php

namespace Madkom\ConsoleBundle\Model;

/**
 * Class Table
 *
 * Model reprezentujacy dane tabelaryczne
 *
 * @package Madkom\ConsoleBundle\Model
 */
class Table {

	/**
	 * @var array
	 */
	public $header;

	/**
	 * @var array
	 */
	public $rows;

	/**
	 * @param array $header
	 */
	public function setHeader($header) {
		$this->header = $header;
	}

	/**
	 * @return array
	 */
	public function getHeader() {
		return $this->header;
	}

	/**
	 * @param array $rows
	 */
	public function setRows($rows) {
		$this->rows = $rows;
	}

	/**
	 * @return array
	 */
	public function getRows() {
		return $this->rows;
	}

	/**
	 * @param array
	 */
	public function addRow($row) {
		$this->rows[] = $row;
	}



}