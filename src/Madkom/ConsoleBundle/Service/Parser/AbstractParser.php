<?php

namespace Madkom\ConsoleBundle\Service\Parser;

use Madkom\ConsoleBundle\Model\Table;
use Madkom\ConsoleBundle\Service\Parser\ParserInterface;

/**
 * Class AbstractParser
 *
 * Klasa abstrakcyjna i implementujaca interfejs ParserInterface,
 * dostarcza podstawowe metody ladowania plikow i operacji z modelem tabeli
 *
 * @package Madkom\ConsoleBundle\Service\Parser
 */
abstract class AbstractParser implements ParserInterface {

	/**
	 * @var string dane odczytane z pliku
	 */
	protected $data;

	/**
	 * @var \Madkom\ConsoleBundle\Model\Table
	 */
	protected $tableModel;

	/**
	 * Odczyt danych z pliku i konwersja do modelu
	 *
	 * @param resource $fileHandle
	 *
	 * @return mixed|void
	 * @throws \Exception
	 */
	public function loadData($fileHandle) {
		if ($fileHandle !== TRUE) {
			throw new \Exception("Błąd odczytu pliku");
		}
		$meta_data = stream_get_meta_data($fileHandle);
		$this->data = file_get_contents($meta_data['uri']);
		$this->convertToModel();
	}

	protected function convertToModel() {

	}

	/**
	 * Wrzucenie modelu tabeli z innego parsera
	 *
	 * @param Table $tableModel
	 *
	 * @return $this
	 */
	public function loadModel(Table $tableModel) {
		$this->tableModel = $tableModel;
		return $this;
	}

	/**
	 * Zwraca model tabeli
	 *
	 * @return Table
	 * @throws \Exception
	 */
	public function getModelRepresentation() {
		if (!$this->tableModel instanceof \Madkom\ConsoleBundle\Model\Table) {
			throw new \Exception("Brak wygenerowane modelu danych");
		}
		return $this->tableModel;
	}
	public function parse() {}

}