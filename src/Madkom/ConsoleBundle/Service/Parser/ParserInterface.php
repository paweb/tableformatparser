<?php


namespace Madkom\ConsoleBundle\Service\Parser;

use Madkom\ConsoleBundle\Model\Table;

/**
 * Class ParserInterface
 *
 * Interfejs Parsera, kazdy nowy parser musi go implementowac
 *
 * @package Madkom\ConsoleBundle\Service\Parser
 */
interface ParserInterface {

	/**
	 * Odczyt danych z pliku i konwersja do modelu
	 *
	 * @param \resource $fileHandle
	 *
	 * @return mixed
	 */
	public function loadData($fileHandle);

	/**
	 * Zwraca model tabeli
	 *
	 * @return \Madkom\ConsoleBundle\Model\Table
	 */
	public function getModelRepresentation();

	/**
	 * Wrzucenie modelu tabeli z innego parsera
	 *
	 * @param Table $tableModel
	 *
	 */
	public function loadModel(Table $tableModel);

	/**
	 * Przekszałcenie modelu tabeli do formatu wyjsciowego danego parsera
	 *
	 * @return string
	 */
	public function parse();

}