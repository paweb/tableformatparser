<?php

namespace Madkom\ConsoleBundle\Service\Parser;


use Madkom\ConsoleBundle\Service\Parser\AbstractParser;
use Madkom\ConsoleBundle\Model\Table;

/**
 * Class CsvParser
 *
 * Parser formatow CSV
 *
 * Format akceptowalny dla CSV:
 *
 * 	'First Name','Last Name','Score'
 *	'Jill','Smith','disqualified'
 *	'Eve','Jackson','94'
 *
 * @package Madkom\ConsoleBundle\Service\Parser
 */
class CsvParser extends AbstractParser{

	/**
	 * Nadpisujemy metode load, w przypadku CSV lepiej pobierac plik linia po linii
	 *
	 * @param resource $fileHandle
	 *
	 * @throws \Exception
	 */
	public function loadData($fileHandle) {
		if ($fileHandle === FALSE) {
			throw new \Exception("Błąd odczytu pliku");
		}
		$header = NULL;
		$data = array();
		while (($row = fgetcsv($fileHandle, 1000, ',', "'")) !== FALSE)
		{
			if(!$header) {
				$header = $row;
			} else {
				$data[] = array_combine($header, $row);
			}
		}
		$this->data = $data;
		$this->convertToModel();
	}

	protected function convertToModel() {
		$this->tableModel = new Table();
		$this->tableModel->setHeader(array_keys($this->data[0]));
		foreach($this->data as $row) {
			$this->tableModel->addRow(array_values($row));
		}
	}

	/**
	 * Parsujemy model tabeli do stringa z CSV
	 * delimiter ustawiony na ,
	 * enclosure na '
	 *
	 * @return string|void
	 */
	public function parse () {
		//Parsowanie naglowka
		$outputString = "";
		$outputString .= join(',', array_map(function($string){
			return sprintf("'%s'", stripcslashes($string));
		}, $this->tableModel->getHeader())).PHP_EOL;
		foreach ($this->tableModel->getRows() as $row) {
			$outputString .= join(',', array_map(function($string){
				return sprintf("'%s'", stripcslashes($string));
			}, $row)).PHP_EOL;
		}
		return $outputString;
	}
}