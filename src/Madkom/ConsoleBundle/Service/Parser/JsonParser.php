<?php

namespace Madkom\ConsoleBundle\Service\Parser;


use Madkom\ConsoleBundle\Service\Parser\AbstractParser;
use Madkom\ConsoleBundle\Model\Table;

/**
 * Class JsonParser
 *
 * Parser formatow JSON
 *
 * Format JSON akceptowany dla tabeli:
 *
 * [{"First Name":"Jill","Last Name":"Smith","Score":"disqualified"},{"First Name":"Eve","Last Name":"Jackson","Score":"94"}]
 *
 * @package Madkom\ConsoleBundle\Service\Parser
 */
class JsonParser extends AbstractParser{

	protected function convertToModel() {
		$jsonData = json_decode($this->data, true);
		$this->tableModel = new Table();
		$this->tableModel->setHeader(array_keys($jsonData[0]));
		foreach($jsonData as $row) {
			$this->tableModel->addRow(array_values($row));
		}
	}

	public function parse () {
		$tableArray = array();
		$header = $this->tableModel->getHeader();
		foreach($this->tableModel->getRows() as $row) {
			$rowWithHeaders = array();
			foreach($row as $key => $value) {
				$rowWithHeaders[$header[$key]]=$value;
			}
			$tableArray[]=$rowWithHeaders;
		}
		return json_encode($tableArray);
	}
}