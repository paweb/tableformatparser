<?php


namespace Madkom\ConsoleBundle\Service;

use Madkom\ConsoleBundle\Service\Parser\ParserInterface;

/**
 * Class Converter
 *
 * Dostepne jako Service
 *
 * Biblioteka do konwersji z danego typu danych tabelarycznych na inny
 *
 * @package Madkom\ConsoleBundle\Service
 */
class Converter {

	/**
	 * @var ParserInterface $fromParser
	 */
	protected $fromParser;

	/**
	 * @var ParserInterface $toParse
	 */
	protected $toParser;

	/**
	 * Dostępne parsery
	 * @var array
	 */
	protected $availableParser;

	public function __construct($availableParser) {
		$this->availableParser = $availableParser;
	}

	/**
	 * Głowna metoda konwertujaca, wczesniej musimy zdefiniowac parser from i out
	 *
	 * @param resource $inputFilePath handler do pliku
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function convert($inputFilePath) {
		if (!file_exists($inputFilePath)) {
			throw new \Exception("Brak pliku");
		}
		$inputFileHandle = fopen($inputFilePath, "r");
		if (!$this->fromParser instanceof ParserInterface) {
			throw new \Exception("Brak parsera");
		}
		$this->fromParser->loadData($inputFileHandle);
		return $this->toParser->loadModel($this->fromParser->getModelRepresentation())->parse();
	}

	/**
	 * Ustawia nam parser dla pliku wejsciowego
	 *
	 * @param $fromParser
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function setFromParser($fromParser) {
		if (array_key_exists($fromParser, $this->availableParser)) {
			$this->fromParser = new $this->availableParser[$fromParser];
		} else {
			throw new \Exception("Format {$fromParser} nie jest obsługiwany");
		}
		return $this;
	}

	/**
	 * @return ParserInterface
	 */
	public function getFromParser() {
		return $this->fromParser;
	}

	/**
	 * Ustawia nam parser dla pliku wyjsciowego
	 *
	 * @param $toParser
	 *
	 * @return $this
	 * @throws \Exception
	 */
	public function setToParser($toParser) {
		if (array_key_exists($toParser, $this->availableParser)) {
			$this->toParser = new $this->availableParser[$toParser];
		} else {
			throw new \Exception("Format {$toParser} nie jest obsługiwany");
		}
		return $this;
	}


	public function getToParser() {
		return $this->toParser;
	}


}