<?php
/**
 * @author Paweł Galerczyk <pawel.galerczyl@gmail.com
 * @description Polecenie do konwersji plikow
 *
 */

namespace Madkom\ConsoleBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FileCommand
 *
 * Komenda konsolowa do konwersji typow danych tabelarycznych
 *
 * @package Madkom\ConsoleBundle\Command
 */
class FileCommand extends ContainerAwareCommand {

	protected function configure() {

		$this
			->setName('converter:file')
			->setDescription('Konwersja danych z pliku z formatu [from] do formatu [to] i zapisania do pliku wyjsciowego')
			->addArgument(
				'inputPath',
				InputArgument::REQUIRED,
				'Sciezka do pliku wejsciowego'
			)
			->addOption(
				'fromFormat',
				'f',
				InputOption::VALUE_REQUIRED,
				'Format wejsciowego pliku'
			)
			->addOption(
				'toFormat',
				't',
				InputOption::VALUE_REQUIRED,
				'Format wyjściowego pliku'
			);
	}
	protected function execute(InputInterface $input, OutputInterface $output) {
		/** @var \Madkom\ConsoleBundle\Service\Converter $converter */
		$converter = $this->getContainer()->get('Madkom_console.converter');
		$converter->setFromParser($input->getOption('fromFormat'))->setToParser($input->getOption('toFormat'));
		$output->write($converter->convert($input->getArgument('inputPath')));
	}

}